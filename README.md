# Tribool for Octave

This project defines a tribool type for GNU Octave. This type is
analogous to a logical type, but takes on of three possible values:
`false`, `true`, and `indeterminate`.

For now this type is implemented as only a scalar and does not yet
support any operators or conversions.
