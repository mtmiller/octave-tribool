/*

Copyright (C) 2016 Mike Miller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "octave-config.h"
#include "defun-dld.h"
#include "ov-base.h"
#include "utils.h"
#include "variables.h"

enum octave_tribool_t
{
  false_value,
  true_value,
  indeterminate_value
};

static std::ostream&
operator << (std::ostream& os, const octave_tribool_t& t)
{
  os << ((t == indeterminate_value) ? "indeterminate" :
         ((t == true_value) ? "true" : "false"));
  return os;
}

class
octave_tribool : public octave_base_value
{
public:

  octave_tribool (void)
    : octave_base_value (), value (false_value) {}

  octave_tribool (bool b)
    : octave_base_value (), value (b ? true_value : false_value) {}

  octave_tribool (octave_tribool_t b)
    : octave_base_value (), value (b) {}

  octave_tribool (const octave_tribool& b)
    : octave_base_value (), value (b.value) {}

  ~octave_tribool (void) {};

  octave_base_value *clone (void) const { return new octave_tribool (*this); }
  octave_base_value *empty_clone (void) const { return new octave_tribool (); }

  dim_vector dims (void) const { static dim_vector dv (1, 1); return dv; }

  size_t byte_size (void) const { return sizeof (value); }

  bool is_defined (void) const { return true; }

  bool is_constant (void) const { return true; }

  bool is_true (void) const { return value == true_value; }

  octave_value all (int = 0) const { return (value == true_value); }

  octave_value any (int = 0) const { return (value == true_value); }

  octave_tribool_t tribool_value () const { return value; }

  bool print_as_scalar (void) const { return true; }

  void print (std::ostream& os, bool pr_as_read_syntax = false);

  void print_raw (std::ostream& os, bool pr_as_read_syntax = false) const;

  void short_disp (std::ostream& os) const;

  octave_value fast_elem_extract (octave_idx_type n) const;

private:
  octave_tribool_t value;

  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA
};

DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (octave_tribool, "tribool", "tribool");

void
octave_tribool::print (std::ostream& os, bool pr_as_read_syntax)
{
  print_raw (os, pr_as_read_syntax);
  newline (os);
}

void
octave_tribool::print_raw (std::ostream& os, bool /*pr_as_read_syntax*/) const
{
  indent (os);
  os << value;
}

void
octave_tribool::short_disp (std::ostream& os) const
{
  os << value;
}

octave_value
octave_tribool::fast_elem_extract (octave_idx_type n) const
{
  return (n == 0) ? octave_value (value) : octave_value ();
}

static octave_tribool_t
octave_tribool_extract (const octave_value& a)
{
  const octave_tribool& t = dynamic_cast<const octave_tribool&> (a.get_rep ());
  return t.tribool_value ();
}

static void
octave_tribool_register_type (void)
{
  static bool type_loaded = false;

  if (! type_loaded)
    {
      octave_tribool::register_type ();
      type_loaded = true;
    }
}

DEFUN_DLD(tribool, args, ,
          "-*- texinfo -*-\n\
@deftypefn  {Loadable Function} tribool\n\
@deftypefnx {Loadable Function} tribool (@var{x})\n\
Return a tribool scalar value.\n\
@seealso{indeterminate}\n\
@end deftypefn\n\
")
{
  mlock ();
  octave_tribool_register_type ();

  int nargin = args.length ();

  if (nargin > 1)
    print_usage ();

  bool b = false;

  if (nargin == 1)
    if (args(0).type_id () == octave_tribool::static_type_id ())
      {
        octave_tribool_t t = octave_tribool_extract (args(0));
        return octave_value (new octave_tribool (t));
      }
    else
      b = args(0).bool_value ();

  return octave_value (new octave_tribool (b));
}

// PKG_ADD: autoload ("indeterminate", "tribool.oct");
// PKG_DEL: autoload ("indeterminate", "tribool.oct", "remove");

DEFUN_DLD(indeterminate, args, ,
          "-*- texinfo -*-\n\
@deftypefn {Loadable Function} indeterminate\n\
Return an indeterminate tribool scalar value.\n\
@seealso{tribool}\n\
@end deftypefn\n\
")
{
  mlock ();
  octave_tribool_register_type ();

  int nargin = args.length ();

  if (nargin > 0)
    print_usage ();

  return octave_value (new octave_tribool (indeterminate_value));
}

/*

## Tests of basic type information
%!assert (class (tribool (false)), "tribool")
%!assert (class (tribool (true)), "tribool")
%!assert (class (tribool (indeterminate)), "tribool")
%!assert (typeinfo (tribool (false)), "tribool")
%!assert (typeinfo (tribool (true)), "tribool")
%!assert (typeinfo (tribool (indeterminate)), "tribool")
%!assert (length (tribool), 1)
%!assert (numel (tribool), 1)
%!assert (size (tribool), [1, 1])

%!test
%! cond = tribool (false);
%! if (cond)
%!   y = 1;
%! else
%!   y = 0;
%! endif
%! assert (y, 0);

%!test
%! cond = tribool (true);
%! if (cond)
%!   y = 1;
%! else
%!   y = 0;
%! endif
%! assert (y, 1);

%!test
%! cond = tribool (indeterminate);
%! if (cond)
%!   y = 1;
%! else
%!   y = 0;
%! endif
%! assert (y, 0);

*/
