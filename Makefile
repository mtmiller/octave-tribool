# -*- makefile -*-

MKOCTFILE = mkoctfile
OCTAVE = octave

all: tribool.oct PKG_ADD PKG_DEL

check: all
	$(OCTAVE) --norc --no-history --silent --eval "test tribool.cc"

test: check

clean:
	-rm -f *.o *.oct PKG_ADD PKG_DEL

.cc.oct:
	$(MKOCTFILE) $< -o $@

PKG_ADD PKG_DEL: tribool.cc
	sed -n -e 's/.*$@: \(.*\)/\1/p' $^ > $@-t
	mv $@-t $@

.PHONY: all check clean test
.SUFFIXES: .oct
